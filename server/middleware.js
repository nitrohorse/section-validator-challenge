'use strict'

const middleware = (() => {
  const errorHandler = (err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
  }

  const cors = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST')
    next()
  }

  return {
    errorHandler,
    cors
  }
})()

module.exports = middleware
