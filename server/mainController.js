'use strict'

const database = require('./database')
const UserSectionValidator = require('./userSectionValidator')

const mainController = (() => {
  const getAllUserSections = (req, res) => {
    try {
      database.getAllUserSections()
        .then(allUserSections => {
          res.status(200).send(allUserSections)
        }, error => {
          res.status(404).send(error)
        })
    } catch (e) {
      res.status(500).send('Something broke!')
    }
  }

  const getUserSectionAnalysis = (req, res) => {
    try {
      const id = Number(req.params.id)
      database.getUserSection(id)
        .then(userSection => {
          const usValidator = new UserSectionValidator()
          usValidator.decorate('isUser')
          usValidator.decorate('areAnswersFilledOut')
          usValidator.decorate('isStatusCompleted')
          usValidator.decorate('areAnswersNotGuessed')
          usValidator.decorate('areAnswersWithinMaxDuration')
          usValidator.decorate('isNotEssay')
          usValidator.decorate('isNotWithinRange', [3, 33])
          usValidator.validate(userSection)

          res.status(200).send(usValidator.errors)
        }, error => {
          res.status(404).send(error)
        })
    } catch (e) {
      res.status(500).send('Something broke!')
    }
  }

  return {
    getAllUserSections,
    getUserSectionAnalysis
  }
})()

module.exports = mainController
