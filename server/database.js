// Mock database with some sample data for the validator
// The data is a row representing a a user's progress through a section of an exam.

var promise = require("bluebird")

// Sample data:
//   userSectionId: Int, unique Id for this database row.
//   sectionId:     Int, foreign key to the section the user is working on.
//   user:          String, user's name
//   status:        String [ready|inProgress|completed] differnet states that the user's section can exist in
var data = [
  {
    userSectionId: 1, sectionId: 1, user: "admin", status: "completed", sectionType: "math", answers: [
      { label: 1, correct: 1, incorrect: 0, blank: 0, duration: 1.5 },
      { label: 2, correct: 0, incorrect: 1, blank: 0, duration: 3 },
      { label: 3, correct: 0, incorrect: 0, blank: 1, duration: 0 },
      { label: 4, correct: 0, incorrect: 0, blank: 1, duration: 0 },
      { label: 5, correct: 0, incorrect: 1, blank: 0, duration: 0 },
      { label: 6, correct: 0, incorrect: 0, blank: 1, duration: 0 },
      { label: 7, correct: 0, incorrect: 1, blank: 0, duration: 0 },
      { label: 8, correct: 0, incorrect: 0, blank: 1, duration: 0 },
      { label: 9, correct: 0, incorrect: 0, blank: 1, duration: 0 },
      { label: 10, correct: 0, incorrect: 0, blank: 1, duration: 0 }
    ]
  },
  {
    userSectionId: 2, sectionId: 2, user: "demo", status: "completed", sectionType: "verbal", answers: [
      { label: 1, correct: 1, incorrect: 0, blank: 0, duration: 5 },
      { label: 2, correct: 1, incorrect: 0, blank: 0, duration: 10 },
      { label: 3, correct: 1, incorrect: 0, blank: 0, duration: 12 },
      { label: 4, correct: 1, incorrect: 0, blank: 0, duration: 23 },
      { label: 5, correct: 1, incorrect: 0, blank: 0, duration: 7 },
      { label: 6, correct: 1, incorrect: 0, blank: 0, duration: 30 },
      { label: 7, correct: 1, incorrect: 0, blank: 0, duration: 10 },
      { label: 8, correct: 1, incorrect: 0, blank: 0, duration: 15 }
    ]
  },
  {
    userSectionId: 3, sectionId: 3, user: "dan", status: "completed", sectionType: "reading", answers: [
      { label: 1, correct: 1, incorrect: 0, blank: 0, duration: 30 },
      { label: 2, correct: 0, incorrect: 0, blank: 1, duration: 10 },
      { label: 3, correct: 0, incorrect: 0, blank: 1, duration: 12 },
      { label: 4, correct: 0, incorrect: 0, blank: 1, duration: 23 },
      { label: 5, correct: 0, incorrect: 0, blank: 1, duration: 7 }
    ]
  },
  {
    userSectionId: 4, sectionId: 4, user: "dan", status: "completed", sectionType: "essay", answers: [
      { label: 1, correct: 0, incorrect: 0, blank: 1, duration: 3600 }
    ]
  },
  {
    userSectionId: 5, sectionId: 2, user: "dan", status: "completed", sectionType: "verbal", answers: [
      { label: 1, correct: 1, incorrect: 0, blank: 0, duration: 43 },
      { label: 2, correct: 0, incorrect: 1, blank: 0, duration: 5 },
      { label: 3, correct: 0, incorrect: 1, blank: 0, duration: 1 },
      { label: 4, correct: 1, incorrect: 0, blank: 0, duration: .5 },
      { label: 5, correct: 0, incorrect: 1, blank: 0, duration: .5 },
      { label: 6, correct: 1, incorrect: 0, blank: 0, duration: .5 },
      { label: 7, correct: 0, incorrect: 1, blank: 0, duration: 1 },
      { label: 8, correct: 0, incorrect: 1, blank: 0, duration: 1 }
    ]
  }
]

// query for a user's section
// @param Int userSectionId
// @return Promise of JSON object or rejection if nothing is found.
exports.getUserSection = promise.method(function getSection(userSectionId) {
  for (var i = 0; i < data.length; i++) {
    if (data[i].userSectionId === userSectionId) {
      return data[i];
    }
  }
  // throw new Error("section not found: " + userSectionId)
  return promise.reject(`section not found: ${userSectionId}`)
})

// query for all the data:
// @return Promise of JSON array of objects
exports.getAllUserSections = function getAllData() {
  return promise.resolve(data)
}
