## Run Server
* Install tools
  * [Node.js + npm](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/en/)
* Install dependencies
  * `yarn` or `npm i`
* Start server
  * `yarn start` or `npm run start`
  * Navigate to http://localhost:8000
* Run tests
  * `yarn test` or `npm run test`