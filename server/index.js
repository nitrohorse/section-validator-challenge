'use strict'

const express = require('express')

const router = require('./router')
const middleware = require('./middleware')

const app = express()
const port = process.env.PORT || 8000

// must come before using the router
app.use(middleware.cors)
app.use(middleware.errorHandler)

app.use(router)

app.listen(port, () => {
  console.log(`We are live on http://localhost:${port}`)
})
