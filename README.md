# Section Validator Challenge
Verify if a section of an exam is "valid" in terms of if it is real user input or junk that should be ignored.

### User's interaction with our system:

Users sign on to the platform to take practice exams.  Each `exam` has multiple `sections`, and each `section`
has 20+ `questions`, all of which are saved in a database.

E.g.
* Exam: *ISEE Upper Level #1*
  * Section #1: *Math*
    * Question #1: *Solve 15 * 40*
    * ...
  * Section #2: *Verbal*
  * Section #3: *Reading*
  * ...

Users can start a section within an exam, and answer questions.  Their progress through the exam, section, and questions
are tracked in three corresponding tables.

> Mock data for the table that tracks the user's progress through a section (`userSections`) is included
in `src/database.js`.


## Problem:
The system uses completed `userSections` to generate statistics and do analytics.  However, many of the completed
sections are filled with "junk", e.g. a user is just looking around and doesn't fill in any answers because they want to see the results page.  We need a way to identify these invalid results, so we can ignore them in the future.

## Validation Rules:
1. Anything done by an admin or a demo account should be ignored.  These can be identified by the `user` field.
2. A section that has at least 50% blank is invalid.
3. Only a status of `completed` should be considered for validation.  Other status should be skipped. `ready|inProgress|completed`
4. If more than 50% of the questions have a duration < 2 sec.
5. If a single question has a duration > 15 min.
6. Essays are always invalid.  Identified in the `sectionType`
7. All userSections between 11-33 (or some arbitrary range) are invalid due to a bug in the system when those were taken.


## Tasks:
1. Create an expressjs server with the following features:
    1. Route that accepts a `userSectionId` and validates that section.
    2. Validator that implements the **Validation Rules**.
    3. Implement some unit test, with room to add more.
    4. Provide the commands to run the server and the tests.
2. Create a tool using AngularJS (v1.x) with the following features:
    1. Show a list of all sections with a link to focus in one one.
    2. On the report for one:
        1. Indicate if it is valid or not **and show all the reasons why**
    3. Use Bootstrap CSS to make the client look presentable.
