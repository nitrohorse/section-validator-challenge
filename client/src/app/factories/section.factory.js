(() => {
  'use strict'

  angular
    .module('app')
    .factory('SectionFactory', ($log, $http, API_URL, API_SERVICE_USERSECTION,
      API_SERVICE_ANALYSIS) => {

      const section = {}

      section.getAll = () => {
        return $http.get(`${API_URL}/${API_SERVICE_USERSECTION}`)
      }

      section.getAnalysis = id => {
        return $http.get(`${API_URL}/${API_SERVICE_USERSECTION}/${id}/${API_SERVICE_ANALYSIS}`)
      }

      return section
    })

})()
