(() => {
  'use strict'

  angular
    .module('app')
    .component('sectionAnalysis', {
      controller: SectionAnalysisController,
      controllerAs: 'vm',
      templateUrl: 'app/sectionAnalysis/sectionAnalysis.view.html',
    })

  /** @ngInject */
  function SectionAnalysisController($log, $stateParams, $state, SectionFactory) {
    const vm = this

    vm.userSectionId = $stateParams.id

    vm.goToAllSections = () => {
      $state.go('sections')
    }

    const activate = () => {
      SectionFactory.getAnalysis(vm.userSectionId).then(response => {
        vm.sectionErrors = response.data
      })
    }

    activate()
  }

})()
