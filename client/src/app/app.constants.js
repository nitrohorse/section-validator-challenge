(() => {
  'use strict'

  angular
    .module('app')
    .constant('API_URL', 'http://localhost:8000')
    .constant('API_SERVICE_USERSECTION', 'usersection')
    .constant('API_SERVICE_ANALYSIS', 'analysis')

})()
