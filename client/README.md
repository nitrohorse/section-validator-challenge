## Run Client
Thanks to [Angular Gulp Boilerplate](https://github.com/1oginov/Angular-Gulp-Boilerplate) for starter code.
* Install tools
  * [Node.js + npm](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/en/)
  * [Gulp](https://gulpjs.com/)
  * [Bower](https://bower.io/)
* Install development dependencies
  * `yarn` or `npm i`
* Install client-side dependencies
  * `bower i`
* Start client
  * `yarn start` or `npm run start`
  * Navigate to http://localhost:3000
  
## Client Screenshots
![User Sections](https://raw.githubusercontent.com/nitrohorse/section-validator-challenge/master/client/screenshot-1.png)

![User Section Analysis](https://raw.githubusercontent.com/nitrohorse/section-validator-challenge/master/client/screenshot-2.png)